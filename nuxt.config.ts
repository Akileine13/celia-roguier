export default {
  devtools: { enabled: false },
  css: [
    '@/assets/sass/main.sass',
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  ssr: true,
  server: {
    host: "0.0.0.0"
  },
  plugins: [
    '~/plugins/vue-scrollto.js',
  ],
  app: {
    head: {
      title: 'Celia Roguier - Psychologue clinicienne et psychothérapeute'
    }
  }
  // modules: ['nuxt-gtag'],
  // gtag: {
  //   id: ''
  // }
};